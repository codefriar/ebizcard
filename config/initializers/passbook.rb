require 'passbook'

Passbook.configure do |passbook|

  # Path to your wwdc cert file
  passbook.wwdc_cert = Rails.root.join('keys/wwdc.pem')

  # Path to your cert.p12 file
  passbook.p12_certificate = Rails.root.join('keys/passcertificate.pem')

  # Path to the key
  passbook.p12_key = Rails.root.join('keys/passkey.pem')
  
  # Password for your certificate
  passbook.p12_password = 'wEt2yOj1Ry1Phes6Dy2H'
end
