Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
	get "pass", to: "pass#index"
	get "kjp.pkpass", to: "pass#index"
  root to: "pass#index"
  get "df14", to: "visitors#index"
end
