class AddCityAndCountryToPasses < ActiveRecord::Migration
  def change
    add_column :passes, :city, :string
    add_column :passes, :country, :string
  end
end
