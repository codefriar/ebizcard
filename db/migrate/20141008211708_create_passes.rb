class CreatePasses < ActiveRecord::Migration
  def change
    create_table :passes do |t|
      t.string :uuid
      t.string :ip

      t.timestamps
    end
  end
end
