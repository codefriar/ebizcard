class PassController < ApplicationController
	require "uuidtools"

	def index
		pass = Pass.new
		pass.ip = request.env["HTTP_X_FORWARDED_FOR"].try(:split, ',').try(:last) || request.env["REMOTE_ADDR"]
		pass.uuid = UUIDTools::UUID.timestamp_create.to_s
		pass.city = request.location.city
		pass.country = request.location.country_code
		pass.save
		json = <<-eos
		{
		  "formatVersion": 1,
		  "teamIdentifier": "745ST2PM9F",
		  "description": "Kevin Poorman (@Codefriar)",
		  "foregroundColor": "rgb(255, 255, 255)",
		  "backgroundColor": "rgb(204, 85, 0)",
		  "labelColor": "rgb(0, 0, 0)",
		  "serialNumber": "KJP-DF14Card-#{pass.uuid}",
		  "logoText": "Kevin Poorman's Card",
		  "organizationName": "@codefriar",
		  "barcode": {
		  	"altText": "Visit Codefriar.com to learn how this works",
		    "message": "https://cfpb.herokuapp.com/kjp.pkpass",
		    "format": "PKBarcodeFormatPDF417",
		    "messageEncoding": "iso-8859-1"
		  },
		  "beacons": [
		    {
		      "major": 50196,
		      "minor": 17195,
		      "relevantText": "Kevin is nearby! Have y'all met?",
		      "proximityUUID": "EBA8FBA7-AD0E-4889-88CE-D750FBE04AAB"
		    }
		  ],
		  "generic": {
		    "primaryFields": [
		      {
		        "key": "fullName",
		        "label": "Name",
		        "value": "Kevin\\nPoorman"
		      }
		    ],
		    "auxiliaryFields": [
		      {
		        "label": "Twitter",
		        "key": "twitterHandle",
		        "value": "@codefriar"
		      },
		      {
		        "label": "Email",
		        "key": "emailAddress",
		        "value": "kjp@codefriar.com"
		      }
		    ],
		    "secondaryFields": [
		      {
		        "key": "jobTitle",
		        "label": "Title",
		        "value": "Principle Architect @ EDL Consulting"
		      },
		      {
		        "key": "other",
		        "label": "",
		        "value": "Force.com MVP"
		      }
		    ],
		    "backFields": [
		    	{
		    	  "attributedValue": "<a href='https://mobile.twitter.com/codefriar/tweets'>@codefriar</a>",
		    	  "label": "Twitter",
		    	  "key": "twitterLink",
		    	  "value": "https://mobile.twitter.com/codefriar/tweets",
		    	  "dataDetectorTypes": [
		    	    "PKDataDetectorTypeLink"
		    	  ]
		    	},
		    	{
		    	  "attributedValue": "<a href='https://cfpb.herokuapp.com/df14'>My Dreamforce Sessions</a>",
		    	  "label": "My Dreamforce Sessions",
		    	  "key": "dfsessionLink",
		    	  "value": "https://cfpb.herokuapp.com/df14",
		    	  "dataDetectorTypes": [
		    	    "PKDataDetectorTypeLink"
		    	  ]
		    	},
		    	{
		    	  "attributedValue": "<a href='http://www.codefriar.com/'>www.codefriar.com</a>",
		    	  "label": "Web site",
		    	  "key": "websiteLink",
		    	  "value": "http://www.codefriar.com/",
		    	  "dataDetectorTypes": [
		    	    "PKDataDetectorTypeLink"
		    	  ]
		    	},
		    	{
		    	  "attributedValue": "<a href='mailto:kjp@codefriar.com'>kjp@codefriar.com</a>",
		    	  "label": "Email",
		    	  "key": "emailLink",
		    	  "value": "mailto:kjp@codefriar.com",
		    	  "dataDetectorTypes": [
		    	    "PKDataDetectorTypeLink"
		    	  ]
		    	},
		      {
		        "attributedValue": "<a href='phone:9193817128'>919.381.7128</a>",
		        "label": "Call me, Maybe?",
		        "key": "phoneLink",
		        "value": "mailto:kjp@codefriar.com",
		        "dataDetectorTypes": [
		          "PKDataDetectorTypeLink"
		        ]
		      },
		      {
		        "attributedValue": "<a href='sms:9193817128'>919.381.7128</a>",
		        "label": "Text me!",
		        "key": "smsLink",
		        "value": "sms:kjp@codefriar.com",
		        "dataDetectorTypes": [
		          "PKDataDetectorTypeLink"
		        ]
		      },
		      {
		        "label": "Bio",
		        "key": "bio",
		        "value": "Kevin's been working with the Platform since 2008. He has architected and built a number of applications on the platform for fortune 500 companies in the advertising industry. Kevin was recently renewed for a second year as one of less than twenty Force.com MVP’s worldwide. Active in the community as a Developer user group leader, Kevin can also regularly be found blogging at codefriar.wordpress.com, on Twitter (@codefriar), and in the #salesforce IRC channel as well as on the Salesforce Stack Overflow Site. Kevin also created and maintains the ngForce library for writing Angular.js Apps on the platform. Winner of last year's mobile development challenge, Kevin regularly speaks on mobile development using Salesforce1, RubyMotion and / or Ionic."
		      },
		      {
		        "attributedValue": "<a href='https://cfpb.herokuapp.com/KevinPoorman.vcf'>Add to contacts</a>",
		        "label": "Download",
		        "key": "vcardLink",
		        "value": "https://cdpb.herokuapp.com/KevinPoorman.vcf",
		        "dataDetectorTypes": [
		          "PKDataDetectorTypeLink"
		        ]
		      }
		    ]
		  },
		  "passTypeIdentifier": "pass.com.codefriar.df14meetup"
		}
		eos
		pass = Passbook::PKPass.new json

		image_path = Rails.root.join('app/assets/images/')

		# Add multiple files
		pass.addFiles [
			"#{image_path}icon.png",
			"#{image_path}icon@2x.png",
			"#{image_path}logo.png",
			"#{image_path}logo@2x.png",
			"#{image_path}thumbnail.png",
			"#{image_path}thumbnail@2x.png"
		]

		pkpass = pass.file
		send_file pkpass.path, type: 'application/vnd.apple.pkpass', disposition: 'attachment', filename: "kjp.pkpass"

		# Or a stream

		# pkpass = pass.stream
		# send_data pkpass.string, type: 'application/vnd.apple.pkpass', disposition: 'attachment', filename: "MeetCodefriar.pkpass"

	end


end
